# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Historias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateField(verbose_name=b'Fecha de la historia')),
                ('titulo', models.CharField(max_length=250)),
                ('descripcion', models.TextField(null=True, blank=True)),
                ('foto', sorl.thumbnail.fields.ImageField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Historia de simas',
            },
            bases=(models.Model,),
        ),
    ]
