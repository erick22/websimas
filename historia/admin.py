# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Historias


class HistoriaAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'fecha',)
# Register your models here.
admin.site.register(Historias, HistoriaAdmin)
