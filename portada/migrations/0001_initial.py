# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FotoPortada',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto1', models.CharField(max_length=250)),
                ('texto2', models.CharField(max_length=250)),
                ('foto', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
