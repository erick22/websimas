from eventos.models import Evento


def globales(request):
	eventos = Evento.objects.order_by('-fecha')[:2]

	return {'latest_events': eventos} 