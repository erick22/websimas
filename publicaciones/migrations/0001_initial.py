# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Disponibilidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('disponibilidad', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'disponibilidad',
                'verbose_name_plural': 'disponibilidades',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Publicacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('autor', models.CharField(max_length=300, null=True, blank=True)),
                ('edicion', models.CharField(max_length=200, null=True, blank=True)),
                ('fecha', models.DateField(max_length=50, null=True, blank=True)),
                ('lugar', models.CharField(max_length=200, null=True, blank=True)),
                ('editorial', models.CharField(max_length=2000, null=True, blank=True)),
                ('paginas', models.CharField(max_length=50, null=True, blank=True)),
                ('isbn', models.CharField(max_length=50, null=True, blank=True)),
                ('codigo', models.CharField(max_length=50, null=True, blank=True)),
                ('resumen', ckeditor.fields.RichTextField(null=True, blank=True)),
                ('portada', sorl.thumbnail.fields.ImageField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('precio', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('idtipodocumento', models.IntegerField(null=True, blank=True)),
                ('nombre1', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo1', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('nombre2', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo2', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('nombre3', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo3', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('nombre4', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo4', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('nombre5', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo5', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('nombre6', models.CharField(max_length=250, null=True, blank=True)),
                ('archivo6', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('tematicaanterior', models.CharField(max_length=1500, null=True, blank=True)),
                ('actualizado', models.NullBooleanField()),
                ('notas', models.TextField(null=True, blank=True)),
                ('publicar', models.NullBooleanField()),
                ('idtipopublicacion', models.IntegerField(null=True, blank=True)),
                ('publicacion', models.CharField(max_length=500, null=True, blank=True)),
                ('claves', models.CharField(max_length=1500, null=True, blank=True)),
                ('enlace', models.CharField(max_length=500, null=True, blank=True)),
                ('uri', models.CharField(max_length=500, null=True, blank=True)),
                ('cidoc', models.NullBooleanField()),
                ('archivo', models.CharField(max_length=500, null=True, blank=True)),
                ('titulo1', models.CharField(max_length=300, null=True, blank=True)),
                ('titulo2', models.CharField(max_length=300, null=True, blank=True)),
                ('titulo3', models.CharField(max_length=300, null=True, blank=True)),
                ('titulo4', models.CharField(max_length=300, null=True, blank=True)),
                ('titulo5', models.CharField(max_length=300, null=True, blank=True)),
                ('idtematica2', models.IntegerField(null=True, blank=True)),
                ('archivo7', models.CharField(max_length=255, null=True, blank=True)),
                ('nombre7', models.CharField(max_length=255, null=True, blank=True)),
                ('estado', models.CharField(max_length=1, blank=True)),
                ('idpublicacion2', models.IntegerField(null=True, blank=True)),
                ('sinfecha', models.NullBooleanField()),
                ('enportada', models.NullBooleanField()),
                ('iddisponibilidad', models.ForeignKey(verbose_name=b'Disponibilidad', blank=True, to='publicaciones.Disponibilidad', null=True)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name': 'Publicaci\xf3n',
                'verbose_name_plural': 'Publicaciones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tematica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tematica', models.CharField(max_length=300, blank=True)),
                ('uri', models.CharField(max_length=150, blank=True)),
                ('foto', models.CharField(max_length=255, blank=True)),
                ('texto', models.TextField(blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
                ('orden', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Tematica',
                'verbose_name_plural': 'Tematicas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tipopublicacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipopublicacion', models.CharField(max_length=300, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Tipo Publicaci\xf3n',
                'verbose_name_plural': 'Tipos de publicaciones',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='publicacion',
            name='idtematica',
            field=models.ManyToManyField(to='publicaciones.Tematica', null=True, verbose_name=b'Tematicas', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='publicacion',
            name='tipo_publicacion',
            field=models.ForeignKey(blank=True, to='publicaciones.Tipopublicacion', null=True),
            preserve_default=True,
        ),
    ]
