from django.conf.urls import patterns, url
from .views import (ListBooksView, DetailBooksView,
                    ListBookSimasView, ListBookCidocView)


urlpatterns = patterns(
    'publicaciones.views',
    url(r'^publicaciones/$', ListBooksView.as_view(),
        name='publicaciones'),
    url(r'^publicaciones/(?P<id>[0-9]+)/(?P<uri>[-\w]+)/$',
        'DetailBooksView', name='detalle-publicacion'),
    url(r'^publicaciones/simas/$', ListBookSimasView.as_view(),
        name='publicaciones'),
    url(r'^publicaciones/cidoc/$', ListBookCidocView.as_view(),
        name='publicaciones'),
)
