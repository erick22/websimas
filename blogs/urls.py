from django.conf.urls import patterns, url

from .views import ListBlogsView, DetailBlogsView, blogs_filtrados

urlpatterns = patterns('blogs.views',
    url(r'^blogs/$', ListBlogsView.as_view(), name='blogs'),
    url(r'^blogs/(?P<id>[0-9]+)/(?P<uri>[-_\w]+)/$', 'DetailBlogsView', 
    									name='detalle-blog'),
    url(r'^blogs/filtrar/$', 'blogs_filtrados', name='blog-filtro'),

)
