# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '__first__'),
        ('otras', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('blog', models.CharField(max_length=500, null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
                ('foto', sorl.thumbnail.fields.ImageField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('uri', models.CharField(max_length=500, null=True, editable=False, blank=True)),
                ('resumen', ckeditor.fields.RichTextField(null=True, blank=True)),
                ('tags', models.CharField(max_length=300, null=True, blank=True)),
                ('idautor', models.ForeignKey(verbose_name=b'Autor', blank=True, to='otras.Autor', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Catblog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('catblog', models.CharField(max_length=350, null=True, verbose_name=b'Categoria del blog', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='blog',
            name='idcatblog',
            field=models.ForeignKey(verbose_name='Categoria del blog', blank=True, to='blogs.Catblog', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='tematica',
            field=models.ManyToManyField(to='publicaciones.Tematica', null=True, verbose_name='Tematica del blog', blank=True),
            preserve_default=True,
        ),
    ]
