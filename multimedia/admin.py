from django.contrib import admin
from .models import *

class VideoAdmin(admin.ModelAdmin):
	list_display = ('video','codigo','texto','realizacion')
	search_fields = ('video','codigo','texto')

	fields = (('video', 'codigo'), 'texto',('youtube','googleid'), ('claves','cidoc'),
		('duracion','anho'),('realizacion','pais'),'tematica','estado')


# Register your models here.
admin.site.register(Cataudio)
admin.site.register(Audio)
admin.site.register(Estadovideo)
admin.site.register(Video, VideoAdmin)
