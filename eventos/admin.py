from django.contrib import admin
from .models import Evento

class EventoAdmin(admin.ModelAdmin):
	list_display = ('evento','fecha','direccion')
	search_fields = ('evento',)
	
# Register your models here.
admin.site.register(Evento, EventoAdmin)