# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Aliado(models.Model):
    idaliado = models.IntegerField(primary_key=True)
    correo = models.CharField(max_length=255, blank=True)
    direccion = models.CharField(max_length=255, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    logo = models.CharField(max_length=500, blank=True)
    aliado = models.CharField(max_length=255, blank=True)
    uri = models.CharField(max_length=500, blank=True)
    web = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'aliado'

    def __unicode__(self):
        return self.aliado

class Area(models.Model):
    idarea = models.IntegerField(primary_key=True)
    area = models.CharField(max_length=150, blank=True)
    ordenarea = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'area'

    def __unicode__(self):
        return self.area

class Asesoria(models.Model):
    idasesoria = models.IntegerField(primary_key=True)
    asesoria = models.CharField(max_length=350, blank=True)
    asesoria_en = models.CharField(max_length=350, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'asesoria'

    def __unicode__(self):
        return self.asesoria

class Audio(models.Model):
    idaudio = models.IntegerField(primary_key=True)
    audio = models.CharField(max_length=255, blank=True)
    embed = models.TextField(blank=True)
    archivomp3 = models.CharField(max_length=500, blank=True)
    fecha_ini = models.DateField(blank=True, null=True)
    idcataudio = models.ForeignKey('Cataudio', db_column='idcataudio', blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'audio'

    def __unicode__(self):
        return self.audio

class Autor(models.Model):
    idautor = models.IntegerField(primary_key=True)
    autor = models.CharField(max_length=500, blank=True)
    email = models.CharField(max_length=150, blank=True)
    imagen = models.CharField(max_length=500, blank=True)
    class Meta:
        managed = False
        db_table = 'autor'

    def __unicode__(self):
        return self.autor

class Banner(models.Model):
    idbanner = models.IntegerField(primary_key=True)
    banner = models.CharField(max_length=500, blank=True)
    titulo = models.CharField(max_length=500, blank=True)
    publicado = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'banner'

    def __unicode__(self):
        return self.titulo

class Blog(models.Model):
    idblog = models.IntegerField(primary_key=True)
    blog = models.CharField(max_length=500, blank=True)
    idautor = models.ForeignKey(Autor, db_column='idautor', blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    descripcion = models.TextField(blank=True)
    foto = models.CharField(max_length=500, blank=True)
    pie = models.CharField(max_length=80, blank=True)
    video1 = models.CharField(max_length=500, blank=True)
    video2 = models.CharField(max_length=500, blank=True)
    blog_en = models.CharField(max_length=500, blank=True)
    descripcion_en = models.TextField(blank=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    idcatblog = models.ForeignKey('Catblog', db_column='idcatblog', blank=True, null=True)
    resumen_en = models.TextField(blank=True)
    resumen = models.TextField(blank=True)
    tags = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'blog'

    def __unicode__(self):
        return self.blog

class Blogtematica(models.Model):
    idtematica = models.IntegerField(primary_key=True)
    idblog = models.ForeignKey(Blog, db_column='idblog')
    class Meta:
        managed = False
        db_table = 'blogtematica'

    def __unicode__(self):
        return self.idblog.blog

class Boletin(models.Model):
    idboletin = models.CharField(primary_key=True, max_length=32)
    boletin = models.CharField(max_length=120, blank=True)
    logo = models.CharField(max_length=500, blank=True)
    numero = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    idboletinedicion = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'boletin'

    def __unicode__(self):
        return self.boletin

class Boletinedicion(models.Model):
    idboletinedicion = models.IntegerField(primary_key=True)
    boletinedicion = models.CharField(max_length=50, blank=True)
    idboletin = models.ForeignKey(Boletin, db_column='idboletin', blank=True, null=True)
    titulo = models.CharField(max_length=350, blank=True)
    descripcion = models.TextField(blank=True)
    imagen = models.CharField(max_length=500, blank=True)
    boletinedicion_en = models.CharField(max_length=50, blank=True)
    descripcion_en = models.TextField(blank=True)
    titulo_en = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'boletinedicion'

    def __unicode__(self):
        return self.titulo

class Boletinnoticia(models.Model):
    idnoticia = models.IntegerField(primary_key=True)
    noticia = models.CharField(max_length=350, blank=True)
    noticia_en = models.CharField(max_length=350, blank=True)
    fecha = models.DateField(blank=True, null=True)
    texto = models.TextField(blank=True)
    texto_en = models.TextField(blank=True)
    idautor = models.ForeignKey(Autor, db_column='idautor', blank=True, null=True)
    idboletinedicion = models.ForeignKey(Boletinedicion, db_column='idboletinedicion', blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True)
    class Meta:
        managed = False
        db_table = 'boletinnoticia'

    def __unicode__(self):
        return self.noticia

class Cataudio(models.Model):
    idcataudio = models.IntegerField(primary_key=True)
    cataudio = models.CharField(max_length=250, blank=True)
    ordencataudio = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'cataudio'

    def __unicode__(self):
        return self.cataudio

class Catblog(models.Model):
    idcatblog = models.CharField(primary_key=True, max_length=35)
    catblog = models.CharField(max_length=350, blank=True)
    ordencatblog = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'catblog'

    def __unicode__(self):
        return self.catblog

class Cidoc(models.Model):
    idcidoc = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=900, blank=True)
    idtematica = models.IntegerField(blank=True, null=True)
    autor = models.CharField(max_length=300, blank=True)
    edicion = models.CharField(max_length=200, blank=True)
    fecha = models.CharField(max_length=50, blank=True)
    lugar = models.CharField(max_length=200, blank=True)
    editorial = models.CharField(max_length=2000, blank=True)
    paginas = models.CharField(max_length=50, blank=True)
    isbn = models.CharField(max_length=50, blank=True)
    codigo = models.CharField(max_length=50, blank=True)
    resumen = models.TextField(blank=True)
    portada = models.CharField(max_length=900, blank=True)
    precio = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    idtipodocumento = models.IntegerField(blank=True, null=True)
    iddisponibilidad = models.IntegerField(blank=True, null=True)
    nombre1 = models.CharField(max_length=250, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    nombre2 = models.CharField(max_length=250, blank=True)
    archivo2 = models.CharField(max_length=500, blank=True)
    nombre3 = models.CharField(max_length=250, blank=True)
    archivo3 = models.CharField(max_length=500, blank=True)
    nombre4 = models.CharField(max_length=250, blank=True)
    archivo4 = models.CharField(max_length=500, blank=True)
    nombre5 = models.CharField(max_length=250, blank=True)
    archivo5 = models.CharField(max_length=500, blank=True)
    nombre6 = models.CharField(max_length=250, blank=True)
    archivo6 = models.CharField(max_length=500, blank=True)
    tematicaanterior = models.CharField(max_length=1500, blank=True)
    actualizado = models.NullBooleanField()
    notas = models.TextField(blank=True)
    publicar = models.NullBooleanField()
    publicacion = models.NullBooleanField()
    sinfecha = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'cidoc'

    def __unicode__(self):
        return self.titulo

class Cidoc2(models.Model):
    idcidoc = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=900, blank=True)
    idtematica = models.IntegerField(blank=True, null=True)
    autor = models.CharField(max_length=300, blank=True)
    edicion = models.CharField(max_length=200, blank=True)
    lugar = models.CharField(max_length=200, blank=True)
    editorial = models.CharField(max_length=2000, blank=True)
    paginas = models.CharField(max_length=50, blank=True)
    isbn = models.CharField(max_length=50, blank=True)
    codigo = models.CharField(max_length=50, blank=True)
    resumen = models.TextField(blank=True)
    portada = models.CharField(max_length=900, blank=True)
    precio = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    idtipodocumento = models.IntegerField(blank=True, null=True)
    iddisponibilidad = models.ForeignKey('Disponibilidad', db_column='iddisponibilidad', blank=True, null=True)
    nombre1 = models.CharField(max_length=250, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    nombre2 = models.CharField(max_length=250, blank=True)
    archivo2 = models.CharField(max_length=500, blank=True)
    nombre3 = models.CharField(max_length=250, blank=True)
    archivo3 = models.CharField(max_length=500, blank=True)
    nombre4 = models.CharField(max_length=250, blank=True)
    archivo4 = models.CharField(max_length=500, blank=True)
    nombre5 = models.CharField(max_length=250, blank=True)
    archivo5 = models.CharField(max_length=500, blank=True)
    nombre6 = models.CharField(max_length=250, blank=True)
    archivo6 = models.CharField(max_length=500, blank=True)
    tematicaanterior = models.CharField(max_length=1500, blank=True)
    actualizado = models.NullBooleanField()
    notas = models.TextField(blank=True)
    publicar = models.NullBooleanField()
    publicacion = models.NullBooleanField()
    fecha2 = models.DateField(blank=True, null=True)
    sinfecha = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'cidoc2'

    def __unicode__(self):
        return self.titulo

class Clave(models.Model):
    idclave = models.IntegerField(primary_key=True)
    clave = models.CharField(unique=True, max_length=255, blank=True)
    class Meta:
        managed = False
        db_table = 'clave'

    def __unicode__(self):
        return self.clave

class Cobertura(models.Model):
    idcobertura = models.IntegerField(primary_key=True)
    cobertura = models.CharField(max_length=25, blank=True)
    class Meta:
        managed = False
        db_table = 'cobertura'

    def __unicode__(self):
        return self.cobertura

class Comentario(models.Model):
    idcomentario = models.IntegerField(primary_key=True)
    idblog = models.ForeignKey(Blog, db_column='idblog', blank=True, null=True)
    idnoticia = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=80, blank=True)
    comentario = models.TextField(blank=True)
    email = models.CharField(max_length=80, blank=True)
    publicado = models.NullBooleanField()
    fecha = models.DateField(blank=True, null=True)
    ip = models.CharField(max_length=25, blank=True)
    idpublicacion = models.IntegerField(blank=True, null=True)
    idevento = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'comentario'

    def __unicode__(self):
        return self.nombre

class Contacto(models.Model):
    idcontacto = models.IntegerField(primary_key=True)
    contacto = models.CharField(max_length=80, blank=True)
    idtipocontacto = models.ForeignKey('Tipocontacto', db_column='idtipocontacto', blank=True, null=True)
    idarea = models.ForeignKey(Area, db_column='idarea', blank=True, null=True)
    cargo = models.CharField(max_length=60, blank=True)
    email = models.CharField(max_length=80, blank=True)
    enlace = models.CharField(max_length=350, blank=True)
    descripcion = models.TextField(blank=True)
    foto = models.CharField(max_length=500, blank=True)
    ordencontacto = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'contacto'

    def __unicode__(self):
        return self.contacto

class Datobasico(models.Model):
    iddatobasico = models.IntegerField(primary_key=True)
    datobasico = models.CharField(max_length=350, blank=True)
    datobasico_en = models.CharField(max_length=500, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'datobasico'

    def __unicode__(self):
        return self.datobasico

class Departamento(models.Model):
    iddepartamento = models.IntegerField(primary_key=True)
    departamento = models.CharField(max_length=50, blank=True)
    idpais = models.ForeignKey('Pais', db_column='idpais', blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'departamento'

    def __unicode__(self):
        return self.departamento

class Directorio(models.Model):
    iddirectorio = models.IntegerField(primary_key=True)
    directorio = models.CharField(max_length=350, blank=True)
    idinstitucionsub = models.ForeignKey('Institucionsub', db_column='idinstitucionsub', blank=True, null=True)
    siglas = models.CharField(max_length=150, blank=True)
    logotipo = models.CharField(max_length=500, blank=True)
    fundacion = models.SmallIntegerField(blank=True, null=True)
    idarea = models.ForeignKey(Area, db_column='idarea', blank=True, null=True)
    direccion = models.CharField(max_length=600, blank=True)
    idpais = models.ForeignKey('Pais', db_column='idpais', blank=True, null=True)
    ciudad = models.CharField(max_length=100, blank=True)
    perfilinstitucional = models.TextField(blank=True)
    mision = models.TextField(blank=True)
    vision = models.TextField(blank=True)
    objetivos = models.TextField(blank=True)
    redes = models.CharField(max_length=300, blank=True)
    comisiones = models.TextField(blank=True)
    territorio = models.CharField(max_length=1000, blank=True)
    idcobertura = models.ForeignKey(Cobertura, db_column='idcobertura', blank=True, null=True)
    gruposmeta = models.CharField(max_length=500, blank=True)
    acciones = models.CharField(max_length=500, blank=True)
    fecha = models.DateField(blank=True, null=True)
    telefono1 = models.CharField(max_length=30, blank=True)
    telefono2 = models.CharField(max_length=30, blank=True)
    fax = models.CharField(max_length=30, blank=True)
    apartadopostal = models.CharField(max_length=250, blank=True)
    correo1 = models.CharField(max_length=150, blank=True)
    correo2 = models.CharField(max_length=150, blank=True)
    perfilinstitucional_en = models.TextField(blank=True)
    mision_en = models.TextField(blank=True)
    vision_en = models.TextField(blank=True)
    objetivos_en = models.TextField(blank=True)
    comisiones_en = models.TextField(blank=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    directorio_en = models.CharField(max_length=350, blank=True)
    class Meta:
        managed = False
        db_table = 'directorio'

    def __unicode__(self):
        return self.directorio

class Disponibilidad(models.Model):
    iddisponibilidad = models.IntegerField(primary_key=True)
    disponibilidad = models.CharField(max_length=200, blank=True)
    class Meta:
        managed = False
        db_table = 'disponibilidad'

    def __unicode__(self):
        return self.disponibilidad

class Documento(models.Model):
    iddocumento = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=255)
    titulo_en = models.CharField(max_length=255, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    uri = models.CharField(max_length=255, blank=True)
    uri_en = models.CharField(max_length=255, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    portada = models.CharField(max_length=500, blank=True)
    url = models.CharField(max_length=500, blank=True)
    class Meta:
        managed = False
        db_table = 'documento'

    def __unicode__(self):
        return self.titulo

class Enlace(models.Model):
    idenlace = models.IntegerField(primary_key=True)
    organizacion = models.CharField(max_length=250, blank=True)
    enlace = models.CharField(max_length=500, blank=True)
    idpais = models.ForeignKey('Pais', db_column='idpais', blank=True, null=True)
    idioma = models.CharField(max_length=50, blank=True)
    class Meta:
        managed = False
        db_table = 'enlace'

    def __unicode__(self):
        return self.enlace

class Estadovideo(models.Model):
    idestadovideo = models.IntegerField(primary_key=True)
    estadovideo = models.CharField(max_length=30, blank=True)
    class Meta:
        managed = False
        db_table = 'estadovideo'

    def __unicode__(self):
        return self.estadovideo

class Evento(models.Model):
    idevento = models.IntegerField(primary_key=True)
    evento = models.CharField(max_length=400)
    objetivo = models.TextField(blank=True)
    fecha = models.DateField(blank=True, null=True)
    lugar = models.CharField(max_length=400, blank=True)
    contacto = models.CharField(max_length=200, blank=True)
    direccion = models.CharField(max_length=200, blank=True)
    telefono = models.CharField(max_length=35, blank=True)
    fax = models.CharField(max_length=35, blank=True)
    apartado = models.CharField(max_length=25, blank=True)
    correo = models.CharField(max_length=150, blank=True)
    web = models.CharField(max_length=160, blank=True)
    fechaingreso = models.DateField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True)
    logotipo = models.CharField(max_length=500, blank=True)
    estado = models.CharField(max_length=1, blank=True)
    organiza = models.CharField(max_length=200, blank=True)
    evento_en = models.CharField(max_length=200, blank=True)
    objetivo_en = models.TextField(blank=True)
    organiza_en = models.CharField(max_length=200, blank=True)
    lugar_en = models.CharField(max_length=400, blank=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    idpais = models.CharField(max_length=2, blank=True)
    ciudad = models.CharField(max_length=200, blank=True)
    class Meta:
        managed = False
        db_table = 'evento'

    def __unicode__(self):
        return self.evento

class Eventotematica(models.Model):
    idevento = models.ForeignKey(Evento, db_column='idevento')
    idtematica = models.IntegerField(primary_key=True)
    class Meta:
        managed = False
        db_table = 'eventotematica'

    def __unicode__(self):
        return self.idevento.evento

class Financiadores(models.Model):
    idfinanciadores = models.IntegerField(primary_key=True)
    logo = models.CharField(max_length=500, blank=True)
    nombre = models.CharField(max_length=120, blank=True)
    web = models.CharField(max_length=500, blank=True)
    orden = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'financiadores'

    def __unicode__(self):
        return self.nombre

class Idtipopublicacion(models.Model):
    idtipopublicacion = models.IntegerField(primary_key=True)
    tipopublicacion = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'idtipopublicacion'

    def __unicode__(self):
        return self.tipopublicacion

class Imagen(models.Model):
    idimagen = models.IntegerField(primary_key=True)
    imagen = models.CharField(max_length=500, blank=True)
    credito = models.CharField(max_length=500, blank=True)
    claves = models.CharField(max_length=255, blank=True)
    creacion = models.DateTimeField(blank=True, null=True)
    idusuario = models.IntegerField(blank=True, null=True)
    idedicion = models.IntegerField(blank=True, null=True)
    manage = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'imagen'

    def __unicode__(self):
        return self.imagen

class Institucionsub(models.Model):
    idinstitucionsub = models.IntegerField(primary_key=True)
    idinstituciontipo = models.ForeignKey('Instituciontipo', db_column='idinstituciontipo', blank=True, null=True)
    institucionsub = models.CharField(max_length=250, blank=True)
    class Meta:
        managed = False
        db_table = 'institucionsub'

    def __unicode__(self):
        return self.institucionsub

class Instituciontipo(models.Model):
    idinstituciontipo = models.IntegerField(primary_key=True)
    instituciontipo = models.CharField(max_length=250, blank=True)
    class Meta:
        managed = False
        db_table = 'instituciontipo'

    def __unicode__(self):
        return self.instituciontipo

class Masvisto(models.Model):
    idmasvisto = models.CharField(primary_key=True, max_length=150)
    visitas = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'masvisto'

    def __unicode__(self):
        return str(self.visitas)

class Multimedia(models.Model):
    idmultimedia = models.IntegerField(primary_key=True)
    idvideo = models.IntegerField(blank=True, null=True)
    descripcion = models.TextField(blank=True)
    fecha = models.DateField(blank=True, null=True)
    titulo = models.CharField(max_length=200, blank=True)
    descripcion_en = models.TextField(blank=True)
    titulo_en = models.CharField(max_length=200, blank=True)
    portada = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'multimedia'

    def __unicode__(self):
        return self.titulo

class NaUsuario(models.Model):
    idusuario = models.IntegerField(primary_key=True)
    usuario = models.CharField(max_length=48, blank=True)
    passwd = models.CharField(max_length=32, blank=True)
    nombre = models.CharField(max_length=60, blank=True)
    apellido = models.CharField(max_length=60, blank=True)
    email = models.CharField(max_length=100, blank=True)
    descripcion = models.TextField(blank=True)
    registrado = models.DateTimeField(blank=True, null=True)
    rol = models.CharField(max_length=12)
    class Meta:
        managed = False
        db_table = 'na_usuario'

    def __unicode__(self):
        return self.usuario

class Noticia(models.Model):
    idnoticia = models.IntegerField(primary_key=True)
    noticia = models.CharField(max_length=350, blank=True)
    fecha = models.DateField(blank=True, null=True)
    importante = models.NullBooleanField()
    fuente = models.CharField(max_length=100, blank=True)
    urlfuente = models.CharField(max_length=350, blank=True)
    pie = models.CharField(max_length=80, blank=True)
    noticia_en = models.CharField(max_length=350, blank=True)
    texto_en = models.TextField(blank=True)
    estado = models.CharField(max_length=1, blank=True)
    texto = models.TextField(blank=True)
    claves = models.CharField(max_length=255, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    idautor = models.IntegerField(blank=True, null=True)
    idrecurso = models.IntegerField(blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    archivo2 = models.CharField(max_length=500, blank=True)
    foto = models.CharField(max_length=500, blank=True)
    idtematica = models.IntegerField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True)
    credito = models.CharField(max_length=200, blank=True)
    evento = models.CharField(max_length=200, blank=True)
    autor = models.CharField(max_length=200, blank=True)
    idtema = models.IntegerField(blank=True, null=True)
    idperiodico = models.IntegerField(blank=True, null=True)
    resumen = models.TextField(blank=True)
    resumen_en = models.TextField(blank=True)
    idtematica2 = models.IntegerField(blank=True, null=True)
    titarchivo1 = models.CharField(max_length=200, blank=True)
    titarchivo2 = models.CharField(max_length=200, blank=True)
    archivo3 = models.CharField(max_length=255, blank=True)
    class Meta:
        managed = False
        db_table = 'noticia'

    def __unicode__(self):
        return self.noticia

class Noticiatematica(models.Model):
    idnoticia = models.IntegerField(primary_key=True)
    idtematica = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'noticiatematica'

    def __unicode__(self):
        return str(self.idtematica)

class Pagina(models.Model):
    pagina = models.CharField(max_length=500, blank=True)
    pagina_en = models.CharField(max_length=500, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    encabezado = models.CharField(max_length=500, blank=True)
    pie = models.CharField(max_length=60, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    titarchivo1 = models.CharField(max_length=500, blank=True)
    archivo2 = models.CharField(max_length=500, blank=True)
    titarchivo2 = models.CharField(max_length=500, blank=True)
    estado = models.CharField(max_length=1, blank=True)
    uri = models.CharField(max_length=50, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    idpagina = models.IntegerField(primary_key=True)
    class Meta:
        managed = False
        db_table = 'pagina'

    def __unicode__(self):
        return self.pagina

class Pais(models.Model):
    idpais = models.CharField(primary_key=True, max_length=2)
    pais = models.CharField(max_length=80, blank=True)
    region = models.CharField(max_length=2, blank=True)
    telefono = models.CharField(max_length=3, blank=True)
    class Meta:
        managed = False
        db_table = 'pais'

    def __unicode__(self):
        return self.pais

class Pensamiento(models.Model):
    idpensamiento = models.IntegerField(primary_key=True)
    pensamiento = models.CharField(max_length=255, blank=True)
    foto = models.CharField(max_length=255, blank=True)
    texto = models.TextField(blank=True)
    fecha = models.DateField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'pensamiento'

    def __unicode__(self):
        return self.pensamiento

class Publicacion(models.Model):
    idpublicacion = models.IntegerField(primary_key=True)
    idtematica = models.IntegerField(blank=True, null=True)
    autor = models.CharField(max_length=300, blank=True)
    edicion = models.CharField(max_length=200, blank=True)
    fecha = models.CharField(max_length=50, blank=True)
    lugar = models.CharField(max_length=200, blank=True)
    editorial = models.CharField(max_length=2000, blank=True)
    paginas = models.CharField(max_length=50, blank=True)
    isbn = models.CharField(max_length=50, blank=True)
    codigo = models.CharField(max_length=50, blank=True)
    resumen = models.TextField(blank=True)
    portada = models.CharField(max_length=900, blank=True)
    precio = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    idtipodocumento = models.IntegerField(blank=True, null=True)
    iddisponibilidad = models.IntegerField(blank=True, null=True)
    nombre1 = models.CharField(max_length=250, blank=True)
    archivo1 = models.CharField(max_length=500, blank=True)
    nombre2 = models.CharField(max_length=250, blank=True)
    archivo2 = models.CharField(max_length=500, blank=True)
    nombre3 = models.CharField(max_length=250, blank=True)
    archivo3 = models.CharField(max_length=500, blank=True)
    nombre4 = models.CharField(max_length=250, blank=True)
    archivo4 = models.CharField(max_length=500, blank=True)
    nombre5 = models.CharField(max_length=250, blank=True)
    archivo5 = models.CharField(max_length=500, blank=True)
    nombre6 = models.CharField(max_length=250, blank=True)
    archivo6 = models.CharField(max_length=500, blank=True)
    tematicaanterior = models.CharField(max_length=1500, blank=True)
    actualizado = models.NullBooleanField()
    notas = models.TextField(blank=True)
    publicar = models.NullBooleanField()
    idtipopublicacion = models.IntegerField(blank=True, null=True)
    publicacion = models.CharField(max_length=500, blank=True)
    publicacion_en = models.CharField(max_length=300, blank=True)
    claves = models.CharField(max_length=1500, blank=True)
    lugar_en = models.CharField(max_length=300, blank=True)
    resumen_en = models.TextField(blank=True)
    enlace = models.CharField(max_length=500, blank=True)
    titulo1_en = models.CharField(max_length=500, blank=True)
    nombre1_en = models.CharField(max_length=500, blank=True)
    nombre2_en = models.CharField(max_length=500, blank=True)
    nombre3_en = models.CharField(max_length=500, blank=True)
    nombre4_en = models.CharField(max_length=500, blank=True)
    nombre5_en = models.CharField(max_length=500, blank=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    cidoc = models.NullBooleanField()
    archivo = models.CharField(max_length=500, blank=True)
    titulo1 = models.CharField(max_length=300, blank=True)
    titulo2 = models.CharField(max_length=300, blank=True)
    titulo3 = models.CharField(max_length=300, blank=True)
    titulo4 = models.CharField(max_length=300, blank=True)
    titulo5 = models.CharField(max_length=300, blank=True)
    idtematica2 = models.IntegerField(blank=True, null=True)
    archivo7 = models.CharField(max_length=255, blank=True)
    nombre7 = models.CharField(max_length=255, blank=True)
    nombre7_en = models.CharField(max_length=255, blank=True)
    nombre6_en = models.CharField(max_length=255, blank=True)
    estado = models.CharField(max_length=1, blank=True)
    idpublicacion2 = models.IntegerField(blank=True, null=True)
    sinfecha = models.NullBooleanField()
    enportada = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'publicacion'

    def __unicode__(self):
        return self.publicacion

class Publicaciontematica(models.Model):
    idpublicacion = models.IntegerField(primary_key=True)
    idtematica = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'publicaciontematica'

    def __unicode__(self):
        return str(self.idtematica)

class Recurso(models.Model):
    recurso = models.CharField(max_length=200, blank=True)
    descripcion = models.TextField(blank=True)
    recurso_en = models.CharField(max_length=200, blank=True)
    descripcion_en = models.TextField(blank=True)
    idrecurso = models.IntegerField(primary_key=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    class Meta:
        managed = False
        db_table = 'recurso'

    def __unicode__(self):
        return self.recurso

class Recursodocumento(models.Model):
    idrecurso = models.ForeignKey(Recurso, db_column='idrecurso', primary_key=True)
    iddocumento = models.ForeignKey(Documento, db_column='iddocumento', primary_key=True)
    class Meta:
        managed = False
        db_table = 'recursodocumento'

    def __unicode__(self):
        return self.idrecurso.recurso

class Recursotematica(models.Model):
    idrecurso = models.ForeignKey(Recurso, db_column='idrecurso')
    idtematica = models.IntegerField(primary_key=True)
    class Meta:
        managed = False
        db_table = 'recursotematica'

    def __unicode__(self):
        return str(self.idrecurso)

class Servicio(models.Model):
    idservicio = models.IntegerField(primary_key=True)
    servicio = models.CharField(max_length=255, blank=True)
    servicio_en = models.CharField(max_length=255, blank=True)
    descripcion = models.TextField(blank=True)
    descripcion_en = models.TextField(blank=True)
    uri = models.CharField(max_length=500, blank=True)
    uri_en = models.CharField(max_length=500, blank=True)
    class Meta:
        managed = False
        db_table = 'servicio'

    def __unicode__(self):
        return self.servicio

class Sistema(models.Model):
    idsistema = models.IntegerField(primary_key=True)
    sistema = models.CharField(max_length=255, blank=True)
    foto = models.CharField(max_length=255, blank=True)
    url = models.CharField(max_length=255, blank=True)
    fecha = models.DateField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'sistema'

    def __unicode__(self):
        return self.sistema

class Tematica(models.Model):
    idtematica = models.IntegerField(primary_key=True)
    tematica = models.CharField(max_length=300, blank=True)
    uri = models.CharField(max_length=150, blank=True)
    uri_en = models.CharField(max_length=150, blank=True)
    tematica_en = models.CharField(max_length=300, blank=True)
    foto = models.CharField(max_length=255, blank=True)
    texto = models.TextField(blank=True)
    texto_en = models.TextField(blank=True)
    fecha = models.DateField(blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'tematica'

    def __unicode__(self):
        return self.tematica

class Tipocontacto(models.Model):
    idtipocontacto = models.CharField(primary_key=True, max_length=35)
    tipocontacto = models.CharField(max_length=120, blank=True)
    class Meta:
        managed = False
        db_table = 'tipocontacto'

    def __unicode__(self):
        return self.tipocontacto

class Tipopublicacion(models.Model):
    idtipopublicacion = models.IntegerField(primary_key=True)
    tipopublicacion = models.CharField(max_length=300, blank=True)
    tipopublicacion_en = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'tipopublicacion'

    def __unicode__(self):
        return self.tipopublicacion

class Video(models.Model):
    idvideo = models.IntegerField(primary_key=True)
    video = models.CharField(max_length=255, blank=True)
    texto = models.TextField(blank=True)
    url = models.CharField(max_length=300, blank=True)
    claves = models.CharField(max_length=255, blank=True)
    creacion = models.DateTimeField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True)
    idusuario = models.IntegerField(blank=True, null=True)
    preview = models.CharField(max_length=500, blank=True)
    youtube = models.CharField(max_length=500, blank=True)
    idtematica = models.IntegerField(blank=True, null=True)
    cidoc = models.NullBooleanField()
    codigo = models.CharField(max_length=50, blank=True)
    duracion = models.CharField(max_length=50, blank=True)
    anho = models.CharField(max_length=4, blank=True)
    realizacion = models.CharField(max_length=255, blank=True)
    idestadovideo = models.IntegerField(blank=True, null=True)
    pais = models.CharField(max_length=50, blank=True)
    googleid = models.CharField(max_length=500, blank=True)
    fecha = models.DateField(blank=True, null=True)
    titulo2 = models.CharField(max_length=300, blank=True)
    class Meta:
        managed = False
        db_table = 'video'

    def __unicode__(self):
        return self.video

